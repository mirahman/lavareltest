Project Scope: A modern job site for interns, part-time job seekers or contractual employment's


Key features:

1. User's
    - Job seeker
    - Employer
    - Admin
    
2. Profiles
    - Job seeker
    - Company
    
3. Contents
    - Job Posts
    - Job Categories
    - Resumes
    - Cover letter
    - search jobs/interns 
    
4. Interview settings , download resume (pdf)

5. ACL, Caching, Email sending, Notification

6. Socket.io to send instant notification