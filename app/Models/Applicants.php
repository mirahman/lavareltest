<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;

class Applicants extends Model
{
    //
    
    protected $table = "applicant";
    
    public $fillable = ["applicantName","jobID","applicationDate"];
    
    

}
