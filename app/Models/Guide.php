<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;

class Guide extends Model
{
    //
    
    protected $table = "job_guide";
    
    public $fillable = ["jobID","jobGuide"];
    
    

}
