<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;

class Jobs extends Model
{
    //
    
    protected $table = "jobs";
    
    public $fillable = ["jobTitle","jobSummary"];
    
    
    public static function getApplicants()
    {
        
    }
    
    public function guide()
    {
        return $this->hasOne('App\Models\Guide', 'jobID', 'id');

    }
    
    public function applicant()
    {
        return $this->hasMany('App\Models\Applicants', 'jobID', 'id');
    }
    
    public function newApplicant()
    {
        
        //return $this->leftJoin("applicant","jobID" , "=", "id")->get();
        //return $this->hasMany('App\Models\Applicants', 'jobID', 'id');
    }
    
}
