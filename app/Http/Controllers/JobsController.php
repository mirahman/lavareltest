<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Models\Jobs;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class JobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        
            //DB::table("jobs")->where("id", '>=', 1);
            
            //DB::table("jobs")->whereRaw('id >=1 and id<= 20');
        
        
            $jobs = Jobs::where('id', '>=', 1)->paginate(1);
            
            

            return view('jobs', ['jobs' => $jobs]);
    }
    
    
    public function applicants(Request $request, $id)
    {
        
        $applicants = Jobs::join('applicant', function($join)
        {
            $join->on('applicant.jobID', '=', 'jobs.id');
        })->get();
        
        /*
        $applicants = DB::table("applicant")
                        ->join("jobs","jobs.id","=","applicant.jobID")
                        ->get();
        */
        
        foreach($applicants as $obj)
        {
            echo $obj->applicantName."<br />";
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('job.add');
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        $job = [];
        $job['jobTitle'] = $request->input('jobTitle');
        $job['jobSummary'] = $request->input('jobSummary');
        
        Jobs::create($job);
        
        
        return redirect(route('allJobs'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = Jobs::findOrFail($id);
        return view('job.edit', ['id' => $id, 'job' => $job]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id = 0)
    {
        
        $id = $request->input("id");
        
        $job = Jobs::find($id);

        $job->jobTitle = $request->input('jobTitle');
        $job->jobSummary = $request->input('jobSummary');
        
        $job->save();
         
        return redirect(route('allJobs'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         
        $job = Jobs::find($id);

        
        $job->delete();
         
        return redirect(route('allJobs'));
    }
}
