<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'jobs'], function () {
    Route::get('all', ['as' => 'allJobs' , 'uses' => 'JobsController@index']);
    Route::get('add', ['as' => 'addNewJob', 'uses' => 'JobsController@create']);
    Route::get('edit/{id}', ['as' => 'editJob', 'uses' => 'JobsController@edit']);
    Route::post('save', ['as' => 'saveJob', 'uses' => 'JobsController@store']);
    Route::put('update', ['as' => 'updateJob', 'uses' => 'JobsController@update']);
    Route::get('delete/{id}', ['as' => 'deleteJob', 'uses' => 'JobsController@destroy']);
    Route::get('applicants/{id}', ['as' => 'applicants', 'uses' => 'JobsController@applicants']);
});


Route::resource('profile', 'ProfileController');
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
