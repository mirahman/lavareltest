@extends('layout')

@section('title')
All New Job
@endsection

@section('content')


{!! Form::model($job, array('route' => 'updateJob', 'class' => 'form-horizontal',  'id'=> 'export-form', 'files' => true)) !!}

<div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">
                    {!! Form::label('jobTitle', 'Job title') !!}
                </label>
                <div class="col-sm-6">
                    {!! Form::text('jobTitle', $job->jobTitle, array('class' => 'form-control')) !!}
                </div>
            </div>

<div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">
                    {!! Form::label('jobSummary', 'Summary') !!}
                </label>
                <div class="col-sm-6">
                    {!! Form::textarea('jobSummary', $job->jobSummary, array('class' => 'form-control')) !!}
                </div>
            </div>

            {!! Form::submit('Click Me!') !!}

{!! Form::close() !!}

__token();

@endsection
