@extends('layout')

@section('title')
All New Job
@endsection

@section('content')


{!! Form::open(array('route' => 'saveJob', 'class' => 'form-horizontal',  'id'=> 'add-form', 'files' => true)) !!}

<div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">
                    {!! Form::label('jobTitle', 'Job title') !!}
                </label>
                <div class="col-sm-6">
                    {!! Form::text('jobTitle', '', array('class' => 'form-control')) !!}
                </div>
            </div>

<div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">
                    {!! Form::label('jobSummary', 'Summary') !!}
                </label>
                <div class="col-sm-6">
                    {!! Form::textarea('jobSummary', '', array('class' => 'form-control')) !!}
                </div>
            </div>

            {!! Form::submit('Click Me!') !!}

{!! Form::close() !!}

@endsection