@extends('layout')

@section('title')
All Jobs
@endsection

@section('content')
<a href="{{route('addNewJob')}}">Add New Job</a>

@if(count($jobs))
<table width="100%" border="0">
    
    <tr>
        <td>#</td>
        <td>{{trans('common.jobTitle')}}</td>
        <td>Last application Date</td>
        <td>Job Type</td>
        <td>Actions</td>
    </tr>
    
    @foreach($jobs as $job)
    <tr>
        <td>
            {{$job->id}}
        </td>
        <td>
            {{$job->jobTitle}}
            <br />
            {{$job->guide->jobGuide}}
        </td>
        <td>
            {{$job->expiresOn}}
        </td>
        <td>
            {{$job->jobType}}
            <br />
            <?php
             $applicants = $job->applicant;
             if(count($applicants)) {
                 foreach($applicants as $app)
                 {
                     echo $app->applicantName."<br />";
                 }
             }
            ?>
        </td>
        <td>
            <a href="{{route('editJob',$job->id)}}">Edit</a> <a href="{{route('deleteJob',$job->id)}}">Delete</a>
        </td>
        
    </tr>
    @endforeach
</table>

{{$jobs->links()}}

@endif

@endsection